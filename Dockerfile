FROM busybox as build

# This is a straight adaption of https://wiki.alpinelinux.org/wiki/Alpine_Linux_in_a_chroot

# Prerequisites
# TODO: https?
ARG mirror=http://dl-cdn.alpinelinux.org/alpine
ARG arch=x86_64
ARG version=v3.12
RUN wget -O - "${mirror}/${version}/main/${arch}/" | grep -o -E apk-tools-static-[0-9]+\.[0-9]+\.[0-9rc_\-]+ | head -n 1 | cut -c 18- > /apkversion
RUN cat /apkversion
ENV chroot_dir=/target

WORKDIR /
RUN mkdir ${chroot_dir}

# Set up APK
RUN set -x && apkversion="$(cat /apkversion)" && wget ${mirror}/${version}/main/${arch}/apk-tools-static-${apkversion}.apk
RUN tar -xzf apk-tools-static-*.apk

# Install the alpine base installation onto the chroot
RUN ./sbin/apk.static -X ${mirror}/${version}/main -U --allow-untrusted --root ${chroot_dir} --initdb add alpine-base

# Set up the chroot
RUN mkdir -p ${chroot_dir}/etc/apk
RUN echo "${mirror}/${version}/main" > ${chroot_dir}/etc/apk/repositories

FROM scratch as alpine
LABEL maintainer "Brian Cole <docker@brianecole.com>"
COPY --from=build /target /
CMD /bin/sh
