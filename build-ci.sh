#!/bin/sh

set -x

# Inputs:
# * $alpineversion - edge, latest-stable, v3.17 (or whatever)
# * $CI_COMMIT_REF_NAME - master or not
# * $CI_REGISTRY_IMAGE - base name of registry image name to use 
# * $CI_COMMIT_SHORT_SHA - short commit SHA

if test "${CI_COMMIT_REF_NAME}" = "master"
then
    export IMAGE_TAG_SUFFIX=""
else
    export IMAGE_TAG_SUFFIX="-${CI_COMMIT_SHORT_SHA}"

fi
echo "Image tag suffix: ${IMAGE_TAG_SUFFIX}"

if test "${alpineversion}" = "edge"
then
    export FIRST_DESTINATION="--destination ${CI_REGISTRY_IMAGE}:edge${IMAGE_TAG_SUFFIX}"
elif test "${alpineversion}" = "latest-stable"
then
    export FIRST_DESTINATION="--destination ${CI_REGISTRY_IMAGE}:latest${IMAGE_TAG_SUFFIX}"
    export SECOND_DESTINATION="--destination ${CI_REGISTRY_IMAGE}:latest-stable${IMAGE_TAG_SUFFIX}"
else
    export FIRST_DESTINATION="--destination ${CI_REGISTRY_IMAGE}:${alpineversion}${IMAGE_TAG_SUFFIX}"
    export SECOND_DESTINATION="--destination ${CI_REGISTRY_IMAGE}:$(echo ${alpineversion} | sed s/^v//)${IMAGE_TAG_SUFFIX}"
fi
/kaniko/executor --context "${CI_PROJECT_DIR}" --dockerfile "${CI_PROJECT_DIR}/Dockerfile" ${FIRST_DESTINATION} ${SECOND_DESTINATION}
