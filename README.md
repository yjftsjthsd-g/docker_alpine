# docker_alpine

Alpine Linux docker image

This repo follows the official Alpine chroot guide, but packages the result into
a container using buildah. It runs the bootstrap process from a busybox image in
order to avoid circular dependencies.


## Versions, tags

Individual versions of alpine are tagged; ex. :3.12 will give you Alpine 3.15.
Edge is tagged as :edge.
I have pointed :latest at the alpine repo called "latest-stable" (and as a
side-effect of the scripts involved, this is also available as :latest-stable),
and this appears to function approximately as expected.


## TODOs

There's some TODOs scattered around if you grep for them, but some highlights:

* Packages are pulled over unencrypted HTTP, and this is included in the image.
* This gets a different `/etc/os-release` than official images?
* It would be cool to automatically enumerate alpine versions.
* It *might* be worth testing the images before pushing? Iffy value.


## License

This repo is MIT (see LICENSE file), but note that that only covers the contents
of this repo (builds scripts, basically), not the contents of the generated
image!
